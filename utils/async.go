package utils

import (
	"poltavaGasoline/models/mathModels"
	"poltavaGasoline/services/jobService"
)

func AsyncAwaitMatrixToUnit(
	name string,
	m *mathModels.Matrix3D,
	fns ...func(*mathModels.Matrix3D) mathModels.Unit,
) (output []mathModels.Unit) {
	output = make([]mathModels.Unit, len(fns))
	w := jobService.NewWithNameMax(name, len(fns))
	defer w.WaitAndRelease()

	for i := 0; i < len(fns); i++ {
		j := i
		w.Push(func() {
			output[j] = fns[j](m)
		})
	}
	return
}

func AsyncAwaitMatrixWithCoordsToUnit(
	name string,
	m *mathModels.Matrix3D,
	data []mathModels.Unit,
	fns ...func(*mathModels.Matrix3D, mathModels.Unit) mathModels.Unit,
) (output []mathModels.Unit) {
	output = make([]mathModels.Unit, len(fns))
	w := jobService.NewWithNameMax(name, len(fns))
	defer w.WaitAndRelease()

	for i := 0; i < len(fns); i++ {
		j := i
		w.Push(func() {
			output[j] = fns[j](m, data[j])
		})
	}
	return
}
