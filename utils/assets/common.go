package assets

import (
	"os"
	"path"
	"path/filepath"
)

var (
	goPath     = os.Getenv(goPathEnvKey)
	projectDir = path.Join(goPath, golangSrcFolder, projectPackage)
	binDir     = func() string {
		ex, err := os.Executable()
		if err != nil {
			panic(err)
		}
		return filepath.Dir(ex)
	}()

	Path = func() (dest string) {
		dest = path.Join(binDir, assetsFolderName)
		if exist(dest) {
			return
		}

		dest = path.Join(projectDir, assetsFolderName)
		if exist(dest) {
			return
		}

		panic("could not find assets folder")
	}()
)

func Get(file string) string {
	return path.Join(Path, file)
}

func exist(file string) bool {
	_, err := os.Open(file)
	return err == nil || os.IsExist(err)
}
