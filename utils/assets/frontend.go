package assets

import "path"

func FrontendProjectPath() string {
	return path.Join(Path, frontendPath, frontAppName)
}

func FrontendPath() string {
	return path.Join(FrontendProjectPath(), frontAppSource)
}
