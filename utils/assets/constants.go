package assets

const (
	goPathEnvKey    = "GOPATH"
	golangSrcFolder = "src"
	projectPackage  = "poltavaGasoline"

	assetsFolderName = "assets"
	iconsFolder      = "icons"

	pngExtension  = ".png"
	icnsExtension = ".icns"
)

const (
	frontendPath = "frontend"
	frontAppName = "gas-3d"

	frontAppSource = "build"
)
