package assets

import "path"

type (
	iconsSource struct {
		Fuel        iconsSet
		Computing3D iconsSet
	}

	iconsSet struct {
		PNG  string
		ICNS string
	}
)

var (
	Icons = iconsSource{
		Fuel:        setOf("fuel"),
		Computing3D: setOf("3D"),
	}
)

func setOf(name string) iconsSet {
	return iconsSet{
		PNG:  getIcon(name + pngExtension),
		ICNS: getIcon(name + icnsExtension),
	}
}

func getIcon(file string) string {
	return path.Join(Path, iconsFolder, file)
}
