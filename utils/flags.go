package utils

import "flag"

const (
	FlagRebuild      = "rebuild"
	flagRebuildUsage = "Provide this flag to rebuild frontend with npm"

	FlagNodeServer      = "nodesrv"
	flagNodeServerUsage = "Provide this flag to bind application to node server: http://localhost:3000"
)

var (
	_ = flag.Bool(FlagRebuild, false, flagRebuildUsage)
	_ = flag.Bool(FlagNodeServer, false, flagNodeServerUsage)
)

func IsFlagPassed(name string) bool {
	found := false
	flag.Visit(func(f *flag.Flag) {
		if f.Name == name {
			found = true
		}
	})
	return found
}
