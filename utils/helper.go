package utils

func Should(_ error) {
}

func Must(must error) {
	if err := must; err != nil {
		panic(err)
	}
}
