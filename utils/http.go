package utils

import (
	"net"
	"strconv"
)

func LookupPort() string {
	port, err := GetFreePort()
	if err != nil {
		panic(err)
	}
	return ":" + strconv.Itoa(port)
}

func GetFreePort() (int, error) {
	addr, err := net.ResolveTCPAddr("tcp", "localhost:0")
	if err != nil {
		return 0, err
	}

	l, err := net.ListenTCP("tcp", addr)
	if err != nil {
		return 0, err
	}
	defer l.Close()
	return l.Addr().(*net.TCPAddr).Port, nil
}
