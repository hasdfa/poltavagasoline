package utils

import "github.com/labstack/gommon/random"

var (
	ApplicationSecureUserAgent = random.New().String(
		255, random.Alphanumeric, random.Symbols,
	)
)
