package utils

import (
	"github.com/ivpusic/grpool"
	"poltavaGasoline/services/jobService"
)

func WaitAllJobs(schedulers ...jobService.IJobScheduler) {
	pool := grpool.NewPool(10, 10)
	pool.WaitCount(len(schedulers))
	defer pool.Release()

	for _, s := range schedulers {
		pool.JobQueue <- func() {
			defer pool.JobDone()
			s.WaitAndRelease()
		}
	}

	pool.WaitAll()
}
