package utils

func MinOfInt3(i1, i2, i3 int) int {
	if i1 < i2 {
		if i1 < i3 {
			return i1
		}
		return i3
	}
	if i2 < i3 {
		return i2
	}
	return i3
}

func MaxOfInt3(i1, i2, i3 int) int {
	if i1 > i2 {
		if i1 > i3 {
			return i1
		}
		return i3
	}
	if i2 > i3 {
		return i2
	}
	return i3
}
