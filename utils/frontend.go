package utils

import (
	"os"
	"os/exec"
	"poltavaGasoline/utils/assets"
)

func FrontendPull() (err error) {
	return runCmd(
		assets.FrontendProjectPath(),
		"git", "pull",
	)
}

func FrontendBuild() (err error) {
	if err = runCmd(
		assets.FrontendProjectPath(),
		"npm", "install",
	); err != nil {
		return
	}

	return runCmd(
		assets.FrontendProjectPath(),
		"npm", "run", "build",
	)
}

func runCmd(dir, name string, args ...string) error {
	cmd := exec.Command(name, args...)
	cmd.Dir = dir
	cmd.Stderr = os.Stderr

	return cmd.Run()
}
