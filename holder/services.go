package holder

import (
	"poltavaGasoline/services/mathService"
	"poltavaGasoline/services/parseService"
	"poltavaGasoline/services/servingService"
)

var (
	MathService  = mathService.New()
	ParseService = parseService.New()

	FrontendService = servingService.NewIris()
)
