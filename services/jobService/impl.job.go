package jobService

import (
	"fmt"
	"github.com/ivpusic/grpool"
	"time"
)

type jobScheduleService struct {
	id string

	count     int64
	doneCount uint64
	max       uint64

	err      string
	released bool

	start time.Time
	pool  *grpool.Pool
	//mux        *sync.Mutex
}

const (
	defaultWaiter = time.Second
)

func (s *jobScheduleService) Push(job grpool.Job) {
	//defer func() {
	//	if err := recover(); err != nil {
	//		str := fmt.Sprint(err)
	//		fmt.Println(str)
	//	}
	//}()
	if s.released {
		return
	}
	if s.pool == nil {
		return
	}
	s.push(job)
}

func (s *jobScheduleService) Release() {
	//defer func() {
	//	if err := recover(); err != nil {
	//		str := fmt.Sprint(err)
	//		fmt.Println(str)
	//	}
	//}()
	s.release()
}

func (s *jobScheduleService) IsReleased() bool {
	return s.released
}

func (s *jobScheduleService) WaitAll() {
	//defer func() {
	//	if err := recover(); err != nil {
	//		str := fmt.Sprint(err)
	//		fmt.Println(str)
	//	}
	//}()
	if s.released {
		return
	}
	if s.pool == nil {
		return
	}
	s.pool.WaitAll()
}

func (s *jobScheduleService) WaitAndRelease() {
	defer s.Release()
	s.WaitAll()
}

func (s *jobScheduleService) String() string {
	return fmt.Sprintf("[%s] -> done: %d, count: %d, max: %d",
		s.id, s.doneCount, s.count, s.max)
}
