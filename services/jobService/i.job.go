package jobService

import (
	"github.com/google/uuid"
	"github.com/ivpusic/grpool"
	"time"
)

type IJobScheduler interface {
	Push(job grpool.Job)

	IsReleased() bool
	Release()
	WaitAll()
	WaitAndRelease()

	String() string
}

const (
	defaultWorkersNum = 10000
)

func NewWorkersName(name string) IJobScheduler {
	return NewWithNameMax(name, 1)
}

func NewWithMax(max int) IJobScheduler {
	return NewWithNameMax(uuid.New().String(), max)
}

func NewWithNameMax(name string, max int) IJobScheduler {
	return NewWithNameMaxWorkers(name, max, defaultWorkersNum)
}

func NewWithNameMaxWorkers(name string, max int, workers int) IJobScheduler {
	return &jobScheduleService{
		id:  name,
		max: uint64(max),

		start: time.Now(),
		pool:  grpool.NewPool(workers, workers),
	}
}
