package jobService

import (
	"fmt"
	"github.com/ivpusic/grpool"
	"sync/atomic"
	"time"
)

const (
	secondsDiv  = 100000000000
	timeRounder = time.Second / 100
)

func (s *jobScheduleService) push(job grpool.Job) {
	s.pool.WaitCount(1)
	atomic.AddInt64(&s.count, 1)
	JobsList.Store(s.id, s)

	s.pool.JobQueue <- func() {
		//defer func() {
		//	if err := recover(); err != nil {
		//		s.release(fmt.Sprint(err))
		//		fmt.Println(err)
		//	}
		//}()
		defer s.done()
		job()
	}
}

func (s *jobScheduleService) done() {
	if s.released {
		return
	}
	if s.count == 0 {
		return
	}

	atomic.AddInt64(&s.count, -1)
	atomic.AddUint64(&s.doneCount, 1)

	s.pool.JobDone()
	if s.doneCount == s.max {
		s.release()
	}
}

func (s *jobScheduleService) releaseError(err string) {
	s.err = err
	s.release()
}

func (s *jobScheduleService) release() {
	if s.released {
		return
	}
	s.released = true

	for s.count > 0 {
		s.done()
	}
	s.pool.Release()
	s.pool = nil
}

func (s *jobScheduleService) speedPerSecond() float64 {
	return float64(s.doneCount) / time.Now().Sub(s.start).Seconds()
}

func (s *jobScheduleService) status() *ServiceStatus {
	if s.released {
		if s.err == "" {
			return &ServiceStatus{
				Name:      s.id,
				SpentTime: s.spentTime().String(),
				Status:    "Done",
			}
		} else {
			return &ServiceStatus{
				Name:   s.id,
				Status: "Error",
				Info:   s.err,
			}
		}
	}

	return &ServiceStatus{
		Name:   s.id,
		Status: "Working",

		Done: s.doneCount,
		Max:  s.max,

		SpentTime: s.spentTime().String(),
		TimeLeft:  s.leftTime(),

		Percents: (float32(s.doneCount) / float32(s.max)) * 100,
	}
}

func (s *jobScheduleService) spentTime() time.Duration {
	return time.Now().Sub(s.start).Round(timeRounder)
}

func (s *jobScheduleService) leftTime() string {
	str := ""

	seconds := int(float64(s.max-s.doneCount)*s.speedPerSecond()) / secondsDiv
	if seconds >= 60 {
		minutes := seconds / 60
		seconds = seconds % 60
		if minutes >= 60 {
			hours := minutes / 60
			minutes = minutes % 60
			if hours >= 24 {
				days := hours / 24
				hours = hours % 24

				str = fmt.Sprintf("%dd", days) + " "
			}
			str = str + fmt.Sprintf("%dhr", hours) + " "
		}
		str = str + fmt.Sprintf("%dm", minutes) + " "
	}
	str = str + fmt.Sprintf("%ds", seconds)
	return str
}
