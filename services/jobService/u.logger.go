package jobService

import (
	"fmt"
	"github.com/google/uuid"
	"strings"
	"sync"
	"time"
)

const (
	tickTime         = time.Second / 10
	deadlockTickTime = tickTime / 10

	caretSymbol = "\r"

	newLine       = "\n"
	newLineLength = len(newLine)

	doubleNewLine = newLine + newLine
)

var (
	JobsList     = &sync.Map{}
	watchersList = &sync.Map{}
)

type ServiceStatus struct {
	Name   string `json:"name,omitempty"`
	Status string `json:"status,omitempty"`
	Info   string `json:"info,omitempty"`

	Done uint64
	Max  uint64

	Percents float32 `json:"percents,omitempty"`

	SpentTime string `json:"spentTime,omitempty"`
	TimeLeft  string `json:"timeLeft,omitempty"`
}

func (s *ServiceStatus) String() string {
	if s.Status == "Working" {
		return fmt.Sprintf("[%s] (%s => %s) [%s>%s] %.2f%% (%d/%d)", s.Name, s.SpentTime, s.TimeLeft,
			strings.Repeat("=", int(s.Percents/2)),
			strings.Repeat(" ", int((100-s.Percents)/2)),
			s.Percents, s.Done, s.Max)
	} else if s.Status == "Done" {
		return fmt.Sprintf("[%s] Done (%s) => 100%%", s.Name, s.SpentTime)
	} else if s.Status == "Error" {
		return fmt.Sprintf("[%s] Error: %s", s.Name, s.Info)
	}
	return ""
}

func (s *ServiceStatus) SimpleString() string {
	if s.Status == "Working" {
		return fmt.Sprintf("[%s] (%s => %s) %.2f%%", s.Name, s.SpentTime, s.TimeLeft, s.Percents)
	} else if s.Status == "Done" {
		return fmt.Sprintf("[%s] Done (%s) => 100%%", s.Name, s.SpentTime)
	} else if s.Status == "Error" {
		return fmt.Sprintf("[%s] Error: %s", s.Name, s.Info)
	}
	return ""
}

func PushWatcher(watcher func(*ServiceStatus)) (id string) {
	defer watchersList.Store(id, watcher)
	return uuid.New().String()
}

func RemoveWatcher(id string) {
	watchersList.Delete(id)
}

func init() {
	go logger()
	// go deadlockLooker()
}

func logger() {
	fmt.Println("[Background logger] -> Job started")
	defer fmt.Println("[Background logger] -> Job stopped")

	for {
		select {
		case <-time.Tick(tickTime):
			JobsList.Range(func(key, value interface{}) bool {
				j := value.(*jobScheduleService)
				if j != nil {
					status := j.status()
					if j.released {
						JobsList.Delete(key)
					}
					watchersList.Range(func(key, value interface{}) bool {
						value.(func(*ServiceStatus))(status)
						return true
					})
				} else {
					JobsList.Delete(key)
				}
				return true
			})
		}
	}
}

func deadlockLooker() {
	counter := 0

	for {
		select {
		case <-time.Tick(deadlockTickTime):
			JobsList.Range(func(key, value interface{}) bool {
				counter = 0
				return false
			})
			if counter > 1000 {
				panic("deadlock")
			}
			counter++
		}
	}
}
