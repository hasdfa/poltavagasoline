package application

import (
	"github.com/asticode/go-astilectron"
	"poltavaGasoline/holder"
	"poltavaGasoline/utils"
)

type ElectronApplication struct {
	Application *astilectron.Astilectron
	Window      *astilectron.Window
}

func New(options astilectron.Options) *ElectronApplication {
	app, err := astilectron.New(options)
	utils.Must(err)
	return &ElectronApplication{Application: app}
}

func (app *ElectronApplication) Start() (err error) {
	if err = app.Application.Start(); err != nil {
		return
	}
	return
}

func (app *ElectronApplication) Wait() {
	app.Application.Wait()
}

func (app *ElectronApplication) Close() {
	app.Application.Close()
}

func (app *ElectronApplication) ShowWindow(options *astilectron.WindowOptions) (err error) {
	if app.Window != nil {
		if err = app.Window.Close(); err != nil {
			return
		}
	}
	if options == nil {
		options = &astilectron.WindowOptions{
			DisableAutoHideCursor: astilectron.PtrBool(true),
			Center:                astilectron.PtrBool(true),
			Load: &astilectron.WindowLoadOptions{
				UserAgent: utils.ApplicationSecureUserAgent,
			},
		}
	} else if options.Load == nil {
		options.Load = &astilectron.WindowLoadOptions{
			UserAgent: utils.ApplicationSecureUserAgent,
		}
	} else {
		options.Load.UserAgent = utils.ApplicationSecureUserAgent
	}

	app.Window, err = app.Application.NewWindow(
		holder.FrontendService.Url(),
		options,
	)
	if err != nil {
		return
	}

	if err = app.Window.Create(); err != nil {
		return
	}

	if !app.Window.IsShown() {
		return app.Window.Show()
	}

	return nil
}
