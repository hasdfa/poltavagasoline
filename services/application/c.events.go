package application

const (
	EventFileUpload  = "file.upload"
	EventFileRequest = "file.request"

	EventServiceStatusUpdated = "service.status.updated"

	EventReactError = "react.error"
)
