package application

import (
	"fmt"
	"poltavaGasoline/models"
	"poltavaGasoline/services/jobService"
)

func (app *ElectronApplication) JobWatcher(status *jobService.ServiceStatus) {
	if err := app.Window.SendMessage(models.Event{
		Event: EventServiceStatusUpdated,
		Name:  status.Name,
		Data:  status,
	}); err != nil {
		fmt.Println(err)
	}
}
