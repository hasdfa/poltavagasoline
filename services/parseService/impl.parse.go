package parseService

import (
	"bytes"
	"github.com/go-gl/mathgl/mgl32"
	"io/ioutil"
	"poltavaGasoline/models/mathModels"
	"poltavaGasoline/models/matrix"
	"poltavaGasoline/services/jobService"
)

type parseService struct {
}

func New() IParseService {
	return &parseService{}
}

func (s *parseService) ReadXYZFile(file string) (*mathModels.PointsSet, error) {
	bts, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}

	return s.ParsePointCloud(bts), nil
}

func (s *parseService) ParsePointCloud(bts []byte) (set *mathModels.PointsSet) {
	lines := bytes.Split(bts, endl)
	set = mathModels.NewPointSet()

	parseJob := jobService.NewWithNameMax("Parse points", len(lines))
	for _, l := range lines {
		btsL := l
		parseJob.Push(func() {
			set.ParseNew(string(btsL))
		})
	}
	parseJob.WaitAll()
	return set
}

func (s *parseService) Fill3D(set *mathModels.PointsSet) *matrix.M3D {
	minX := -set.MinX
	minY := -set.MinY
	minZ := -set.MinZ

	m := matrix.New(
		set.MaxX+minX,
		set.MaxY+minY,
		set.MaxZ+minZ,
	)

	parseJob := jobService.NewWithNameMax("Fill MatrixObj", int(set.Length))

	set.Source.Range(func(key, value interface{}) bool {
		parseJob.Push(func() {
			p := value.(*mathModels.Point)
			defer func() { p = nil }()
			m.Set(
				p.X+minX,
				p.Y+minY,
				p.Z+minZ,
				p.RawReflection,
			)
		})
		return true
	})
	parseJob.WaitAll()
	return m
}

func (s *parseService) FillMatrixObj(set *mathModels.PointsSet) mathModels.IMatrix {
	m := mathModels.NewMatrix()

	m.MMinX = mathModels.Unit(mgl32.Abs(float32(set.MinX)))
	m.MMinY = mathModels.Unit(mgl32.Abs(float32(set.MinY)))
	m.MMinZ = mathModels.Unit(mgl32.Abs(float32(set.MinZ)))

	m.MMaxX = set.MaxX + m.MMinX
	m.MMaxY = set.MaxY + m.MMinY
	m.MMaxZ = set.MaxZ + m.MMinZ

	m.X = make([]*mathModels.XAxis, m.MMaxX)
	parseJob := jobService.NewWithNameMax("Fill MatrixObj", int(set.Length))

	set.Source.Range(func(key, value interface{}) bool {
		p := value.(*mathModels.Point)
		parseJob.Push(func() {
			defer func() { p = nil }()
			if p == nil {
				return
			}
			m.Set(
				p.X+m.MMinX,
				p.Y+m.MMinY,
				p.Z+m.MMinZ,
				p.RawReflection,
			)
		})
		return true
	})
	parseJob.WaitAll()
	return m
}

func (s *parseService) FillMatrix3D(set *mathModels.PointsSet) mathModels.IMatrix {
	minX := -set.MinX
	minY := -set.MinY
	minZ := -set.MinZ

	m := mathModels.New3D(
		set.MaxX+minX,
		set.MaxY+minY,
		set.MaxZ+minZ,
	)

	parseJob := jobService.NewWithNameMax("Fill MatrixObj", int(set.Length))

	set.Source.Range(func(key, value interface{}) bool {
		parseJob.Push(func() {
			p := value.(*mathModels.Point)
			defer func() { p = nil }()
			m.Set(
				p.X+minX,
				p.Y+minY,
				p.Z+minZ,
				p.RawReflection,
			)
		})
		return true
	})
	parseJob.WaitAll()
	return m
}
