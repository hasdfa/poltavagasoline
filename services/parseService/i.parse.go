package parseService

import (
	"poltavaGasoline/models/mathModels"
	"poltavaGasoline/models/matrix"
)

type IParseService interface {
	ReadXYZFile(file string) (*mathModels.PointsSet, error)
	ParsePointCloud([]byte) *mathModels.PointsSet

	Fill3D(set *mathModels.PointsSet) *matrix.M3D
	FillMatrix3D(*mathModels.PointsSet) mathModels.IMatrix
	FillMatrixObj(*mathModels.PointsSet) mathModels.IMatrix
}

var (
	endl = []byte("\r\n")
)
