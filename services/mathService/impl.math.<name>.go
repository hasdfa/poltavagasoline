package mathService

import (
	"poltavaGasoline/models/mathModels"
	"poltavaGasoline/models/matrix"
	"poltavaGasoline/services/jobService"
)

//     y   ___________
//       /|          /|
//      / |         / |
//     /__________ /  |
//    |   |       |   |
//    |   |_______|__ | x
//    |  /        |  /
//    | /         | /
//    |/__________|/
//   z

func (s *mathService) CountFromCenter(matrix *matrix.M3D) uint64 {
	return s.CountFrom(mathModels.NewRawPoint(
		matrix.MaxX/2,
		matrix.MaxY/2,
		matrix.MaxZ/2,
	), matrix)
}

func (s *mathService) CountFrom(from mathModels.RawPoint, m *matrix.M3D) uint64 {
	w := jobService.NewWorkersName("CountFrom")

	s.walk(w, m, from)
	w.WaitAndRelease()

	return s.Counter
}

func (s *mathService) walk(w jobService.IJobScheduler, m *matrix.M3D, p mathModels.RawPoint) {
	point := m.Get(p.X(), p.Y(), p.Z())

	point.Lock()
	defer point.Unlock()

	if point.Visited {
		return
	}
	point.Visited = true

	if m.Exists(p.X(), p.Y(), p.Z()) {
		return
	}
	s.Push()

	w.Push(func() { s.walk(w, m, p.AddX(-1)) })
	w.Push(func() { s.walk(w, m, p.AddX(1)) })
	w.Push(func() { s.walk(w, m, p.AddY(-1)) })
	w.Push(func() { s.walk(w, m, p.AddY(1)) })
	w.Push(func() { s.walk(w, m, p.AddZ(-1)) })
	w.Push(func() { s.walk(w, m, p.AddZ(1)) })

	w.Push(func() { s.walk(w, m, p.AddX(-1).AddY(-1)) })
	w.Push(func() { s.walk(w, m, p.AddX(-1).AddY(1)) })
	w.Push(func() { s.walk(w, m, p.AddX(-1).AddZ(-1)) })
	w.Push(func() { s.walk(w, m, p.AddX(-1).AddZ(1)) })
	w.Push(func() { s.walk(w, m, p.AddX(1).AddY(-1)) })
	w.Push(func() { s.walk(w, m, p.AddX(1).AddY(1)) })
	w.Push(func() { s.walk(w, m, p.AddX(1).AddZ(-1)) })
	w.Push(func() { s.walk(w, m, p.AddX(1).AddZ(1)) })
	w.Push(func() { s.walk(w, m, p.AddY(1).AddZ(1)) })
	w.Push(func() { s.walk(w, m, p.AddY(1).AddZ(-1)) })
	w.Push(func() { s.walk(w, m, p.AddY(-1).AddZ(1)) })
	w.Push(func() { s.walk(w, m, p.AddY(-1).AddZ(-1)) })

	w.Push(func() { s.walk(w, m, p.AddX(1).AddY(1).AddZ(1)) })
	w.Push(func() { s.walk(w, m, p.AddX(1).AddY(1).AddZ(-1)) })
	w.Push(func() { s.walk(w, m, p.AddX(1).AddY(-1).AddZ(1)) })
	w.Push(func() { s.walk(w, m, p.AddX(-1).AddY(1).AddZ(1)) })
	w.Push(func() { s.walk(w, m, p.AddX(1).AddY(-1).AddZ(-1)) })
	w.Push(func() { s.walk(w, m, p.AddX(-1).AddY(1).AddZ(-1)) })
	w.Push(func() { s.walk(w, m, p.AddX(-1).AddY(-1).AddZ(1)) })
	w.Push(func() { s.walk(w, m, p.AddX(-1).AddY(-1).AddZ(-1)) })
}
