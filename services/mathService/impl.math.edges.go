package mathService

import (
	"poltavaGasoline/models/mathModels"
)

//     y   ___________
//       /           /|
//      /   ⬆⬆⬆   / |
//     /__________ /  |
//    |   |       |   |
//    |   |_______|__ | x
//    |  /        |  /
//    | /         | /
//    |/__________|/
//   z

func (s *mathService) findUpperEdge(matrix mathModels.IMatrix) (y mathModels.Unit) {
	x := matrix.MaxX() / 2
	y = matrix.MaxY() / 2
	z := matrix.MaxZ() / 2
	max := matrix.MaxY() - 1

MAIN:
	for ; x < 0; x-- {
		for ; z < 0; z-- {
			blank := true
			for _y := y; _y < matrix.MaxY(); _y++ {
				if matrix.Exists(x, _y, z) {
					blank = false
					break
				}
			}
			if !blank {
				break MAIN
			}
		}
	}

	for ; y < max; y++ {
		if matrix.Exists(x, y+1, z) {
			return y
		}
	}
	return
}

//     y   ___________
//       /           /|
//      /           / |
//     /__________ /  |
//    |   |       |   |
//    |   |_______|__ | x
//    |  /        |  /
//    | /  ⬇⬇⬇  | /
//    |/__________|/
//   z

func (s *mathService) findDownerEdge(matrix mathModels.IMatrix) (y mathModels.Unit) {
	x := matrix.MaxX() / 2
	y = matrix.MaxY() / 2
	z := matrix.MaxZ() / 2

MAIN:
	for ; x < 0; x-- {
		for ; z < 0; z-- {
			blank := true
			for _y := y; _y > 0; _y-- {
				if matrix.Exists(x, _y, z) {
					blank = false
					break
				}
			}
			if !blank {
				break MAIN
			}
		}
	}

	for ; y > 0; y-- {
		if matrix.Exists(x, y-1, z) {
			return y
		}
	}
	return
}

//     y   ___________
//       /           /|
//      /           / |
//     /__________ /  |
//    |<= |       |   |
//    |<= |_______|__ | x
//    |<=/        |  /
//    | /         | /
//    |/__________|/
//   z

func (s *mathService) findLeftEdge(matrix mathModels.IMatrix) (x mathModels.Unit) {
	y := matrix.MaxY() / 2
	z := matrix.MaxZ() / 2

	for x = matrix.MaxX() / 2; x > 0; x-- {
		if !matrix.Exists(x-1, y, z) {
			return
		}
	}
	return
}

//     y   ___________
//       /           /|
//      /           / |
//     /__________ /=>|
//    |   |       | =>|
//    |   |_______|___| x
//    |  /        |=>/
//    | /         | /
//    |/__________|/
//   z

func (s *mathService) findRightEdge(matrix mathModels.IMatrix) (x mathModels.Unit) {
	y := matrix.MaxY() / 2
	z := matrix.MaxZ() / 2
	max := matrix.MaxX() - 1

	for x = matrix.MaxX() / 2; x < max; x++ {
		if !matrix.Exists(x+1, y, z) {
			return
		}
	}
	return
}

//     y   ___________
//       /           /|
//      /   ⬈⬈⬈⬈⬈   / |
//     /__________ /  |
//    |   |       |   |
//    |   |_______|__ | x
//    |  /        |  /
//    | /         | /
//    |/__________|/
//   z
func (s *mathService) findFrontEdge(matrix mathModels.IMatrix) (z mathModels.Unit) {
	x := matrix.MaxX() / 2
	y := matrix.MaxY() / 2

	for z = matrix.MaxZ() / 2; z > 0; z-- {
		if !matrix.Exists(x, y, z-1) {
			return
		}
	}
	return
}

//     y   ___________
//       /           /|
//      /           / |
//     /__________ /  |
//    |   |       |   |
//    |   |_______|__ | x
//    |  /        |  /
//    | /  ⬋⬋⬋⬋⬋  | /
//    |/__________|/
//   z

func (s *mathService) findBackEdge(matrix mathModels.IMatrix) (z mathModels.Unit) {
	x := matrix.MaxX() / 2
	y := matrix.MaxY() / 2

	for z = matrix.MaxZ() / 2; z < matrix.MaxZ(); z++ {
		if !matrix.Exists(x, y, z+1) {
			return
		}
	}
	return
}
