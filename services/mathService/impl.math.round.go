package mathService

import (
	"fmt"
	"poltavaGasoline/models/mathModels"
)

//     y   ___________
//       /           /|
//      /   ⬆⬆⬆   / |
//     /__________ /  |
//    |   |       |   |
//    |   |_______|__ | x
//    |  /        |  /
//    | /         | /
//    |/__________|/
//   z

func (s *mathService) cutUpperEdge(matrix mathModels.IMatrix, y mathModels.Unit) mathModels.Unit {
	var x mathModels.Unit
	var z mathModels.Unit
	var hasBlank = false
	max := matrix.MaxY() - 1

	for ; y < max; y++ {
		hasBlank = false
	flatRange:
		for x = 0; x < matrix.MaxX(); x++ {
			for z = 0; z < matrix.MaxZ(); z++ {
				if !matrix.Exists(x, y+1, z) {
					hasBlank = true
					break flatRange
				}
			}
		}
		if !hasBlank {
			return y
		}
		fmt.Printf("upper: %d %d %d\n", x, y, z)
	}
	return matrix.MaxY()
}

//     y   ___________
//       /           /|
//      /           / |
//     /__________ /  |
//    |   |       |   |
//    |   |_______|__ | x
//    |  /        |  /
//    | /  ⬇⬇⬇  | /
//    |/__________|/
//   z

func (s *mathService) cutDownerEdge(matrix mathModels.IMatrix, y mathModels.Unit) mathModels.Unit {
	var x mathModels.Unit
	var z mathModels.Unit
	var hasBlank = false

	for ; y > 1; y-- {
		hasBlank = false
	flatRange:
		for x = 0; x < matrix.MaxX(); x++ {
			for z = 0; z < matrix.MaxZ(); z++ {
				if !matrix.Exists(x, y-1, z) {
					hasBlank = true
					break flatRange
				}
			}
		}
		if !hasBlank {
			return y
		}
		fmt.Printf("downer: %d %d %d\n", x, y, z)
	}
	return 0
}

//     y   ___________
//       /           /|
//      /           / |
//     /__________ /  |
//    |<= |       |   |
//    |<= |_______|__ | x
//    |<=/        |  /
//    | /         | /
//    |/__________|/
//   z

func (s *mathService) cutLeftEdge(matrix mathModels.IMatrix, x mathModels.Unit) mathModels.Unit {
	var y mathModels.Unit
	var z mathModels.Unit
	var hasBlank = false

	for ; x > 1; x-- {
		hasBlank = false
	flatRange:
		for y = 0; y < matrix.MaxY(); y++ {
			for z = 0; z < matrix.MaxZ(); z++ {
				if !matrix.Exists(x-1, y, z) {
					hasBlank = true
					break flatRange
				}
			}
		}
		if !hasBlank {
			return x
		}
		fmt.Printf("left: %d %d %d\n", x, y, z)
	}
	return 0
}

//     y   ___________
//       /           /|
//      /           / |
//     /__________ /=>|
//    |   |       | =>|
//    |   |_______|___| x
//    |  /        |=>/
//    | /         | /
//    |/__________|/
//   z

func (s *mathService) cutRightEdge(matrix mathModels.IMatrix, x mathModels.Unit) mathModels.Unit {
	var y mathModels.Unit
	var z mathModels.Unit
	var hasBlank = false
	max := matrix.MaxX() - 1

	for ; x < max; x++ {
		hasBlank = false
	flatRange:
		for y = 0; y < matrix.MaxY(); y++ {
			for z = 0; z < matrix.MaxZ(); z++ {
				if !matrix.Exists(x+1, y, z) {
					hasBlank = true
					break flatRange
				}
			}
		}
		if !hasBlank {
			return x
		}
		fmt.Printf("right: %d %d %d\n", x, y, z)
	}
	return matrix.MaxX()
}

//     y   ___________
//       /   ⬈⬈⬈⬈⬈   /|
//      /           / |
//     /__________ /  |
//    |   |       |   |
//    |   |_______|__ | x
//    |  /        |  /
//    | /         | /
//    |/__________|/
//   z

func (s *mathService) cutFrontEdge(matrix mathModels.IMatrix, z mathModels.Unit) mathModels.Unit {
	var x mathModels.Unit
	var y mathModels.Unit
	var hasBlank = false

	for ; z > 1; z-- {
		hasBlank = false
	flatRange:
		for x = 0; x < matrix.MaxX(); x++ {
			for y = 0; y < matrix.MaxY(); y++ {
				if !matrix.Exists(x, y, z-1) {
					hasBlank = true
					break flatRange
				}
			}
		}
		if !hasBlank {
			return z
		}
		fmt.Printf("front: %d %d %d\n", x, y, z)
	}
	return 0
}

//     y   ___________
//       /           /|
//      /           / |
//     /__________ /  |
//    |   |       |   |
//    |   |_______|__ | x
//    |  /        |  /
//    | /  ⬋⬋⬋⬋⬋  | /
//    |/__________|/
//   z

func (s *mathService) cutBackEdge(matrix mathModels.IMatrix, z mathModels.Unit) mathModels.Unit {
	var x mathModels.Unit
	var y mathModels.Unit
	var hasBlank = false
	max := matrix.MaxZ() - 1

	for ; z < max; z++ {
		hasBlank = false
	flatRange:
		for x = 0; x < matrix.MaxX(); x++ {
			for y = 0; y < matrix.MaxY(); y++ {
				if !matrix.Exists(x, y, z+1) {
					hasBlank = true
					break flatRange
				}
			}
		}
		if !hasBlank {
			return z
		}
		fmt.Printf("back: %d %d %d\n", x, y, z)
	}
	return matrix.MaxZ()
}
