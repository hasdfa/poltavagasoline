package mathService

import (
	"poltavaGasoline/models/mathModels"
	"poltavaGasoline/models/matrix"
	"sync/atomic"
)

type IMathService interface {
	CutCubeData(matrix mathModels.IMatrix) (*mathModels.Point, *mathModels.Point)

	CountFromCenter(m *matrix.M3D) uint64
	CountFrom(point mathModels.RawPoint, m *matrix.M3D) uint64
}

type mathService struct {
	Counter uint64
}

func (s *mathService) Push() {
	atomic.AddUint64(&s.Counter, 1)
}

func New() IMathService {
	return &mathService{}
}
