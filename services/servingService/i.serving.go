package servingService

type (
	IServingService interface {
		Handle(pattern, dir string)

		Url() string
		Port() string

		Serve() error
		Close() error
	}
)

const (
	localhostUrl = "http://127.0.0.1"
)
