package servingService

import (
	stdContext "context"
	"fmt"
	"github.com/kataras/iris"
	irisContext "github.com/kataras/iris/context"
	"poltavaGasoline/utils"
)

type irisServingService struct {
	app    *iris.Application
	port   string
	reload func()
}

func NewIris() IServingService {
	srvc := &irisServingService{
		app: iris.Default(),
	}
	srvc.app.Use(func(ctx irisContext.Context) {
		if ctx.Request().UserAgent() == utils.ApplicationSecureUserAgent {
			ctx.Next()
		}
	})
	return srvc
}

func (s *irisServingService) Url() string {
	if utils.IsFlagPassed(utils.FlagNodeServer) {
		return "http://localhost:3000"
	}
	if s.port == "" {
		s.port = utils.LookupPort()
	}
	return localhostUrl + s.Port()
}

func (s *irisServingService) Port() string {
	return s.port
}

func (s *irisServingService) Handle(pattern, dir string) {
	fmt.Printf("Handle [%s] -> %s\n", pattern, dir)
	s.app.StaticWeb(pattern, dir)
}

func (s *irisServingService) Serve() error {
	return s.app.Run(iris.Addr(s.Port()))
}

func (s *irisServingService) Close() error {
	return s.app.Shutdown(stdContext.Background())
}
