package models

import "github.com/mitchellh/mapstructure"

type Event struct {
	Event string      `json:"event,omitempty"`
	Name  string      `json:"name,omitempty"`
	Data  interface{} `json:"data,omitempty"`
}

func (e *Event) UnmarshalData(obj interface{}) error {
	return mapstructure.Decode(e.Data, obj)
}
