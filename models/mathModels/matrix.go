package mathModels

type (
	MatrixObj struct {
		X []*XAxis

		MMinX Unit
		MMinY Unit
		MMinZ Unit

		MMaxX Unit
		MMaxY Unit
		MMaxZ Unit

		Upper  *Point
		Downer *Point
		Left   *Point
		Right  *Point
		Front  *Point
		Back   *Point
	}

	XAxis struct {
		Y []*YAxis
	}

	YAxis struct {
		Z []*ZAxis
	}

	ZAxis struct {
		RawReflection Unit
	}
)

func NewMatrix() *MatrixObj {
	return &MatrixObj{}
}

func (m *MatrixObj) Length() int {
	return int(m.MMaxX * m.MMaxY * m.MMaxZ)
}

func (m *MatrixObj) MaxX() Unit {
	return m.MMaxX
}

func (m *MatrixObj) MaxY() Unit {
	return m.MMaxY
}

func (m *MatrixObj) MaxZ() Unit {
	return m.MMaxZ
}

func (m *MatrixObj) get(x, y, z Unit) (za *ZAxis) {
	if x < 0 || y < 0 || z < 0 {
		return
	}
	if m.X == nil {
		return
	}
	if len(m.X) < int(x+1) {
		return
	}
	xp := m.X[x]
	if xp == nil {
		return
	}

	if xp.Y == nil {
		return
	}
	if len(xp.Y) <= int(y+1) {
		return
	}
	yp := xp.Y[y]
	if yp == nil {
		return
	}

	if yp.Z == nil {
		return
	}
	if len(yp.Z) <= int(z+1) {
		return
	}
	return yp.Z[z]
}

func (m *MatrixObj) Get(x, y, z Unit) (v Unit) {
	return m.get(x, y, z).RawReflection
}

func (m *MatrixObj) Exists(x, y, z Unit) bool {
	return m.get(x, y, z) != nil
}

// XYZ
func (m *MatrixObj) Set(x, y, z, value Unit) {
	if m == nil || value == 0 {
		return
	} // skip

	if m.X[x] == nil {
		m.X[x] = &XAxis{}
	}

	if m.X[x].Y == nil {
		m.X[x].Y = make([]*YAxis, m.MMaxY)
	}
	if m.X[x].Y[y] == nil {
		m.X[x].Y[y] = &YAxis{}
	}

	if m.X[x].Y[y].Z == nil {
		m.X[x].Y[y].Z = make([]*ZAxis, m.MMaxZ)
	}

	m.X[x].Y[y].Z[z] = &ZAxis{
		RawReflection: Max(value, 1),
	}
}
