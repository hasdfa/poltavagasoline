package mathModels

import (
	"fmt"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
)

type (
	PointsSet struct {
		Source *sync.Map
		Length int64

		MaxX Unit
		MinX Unit

		MaxY Unit
		MinY Unit

		MaxZ Unit
		MinZ Unit
	}

	Point struct {
		X             Unit
		Y             Unit
		Z             Unit
		RawReflection Unit
	}
)

var (
	space        = " "
	id    uint64 = 0
)

func NewPointSet() *PointsSet {
	return &PointsSet{
		Source: &sync.Map{},
		Length: 0,
	}
}

func (set *PointsSet) ParseNew(str string) {
	p := set.MakePoint(str)
	if p != nil {
		set.Source.Store(atomic.AddUint64(&id, 1), p)
		atomic.AddInt64(&set.Length, 1)
	}
}

func (set *PointsSet) MakePoint(str string) (p *Point) {
	defer func() {
		go func(pt *Point) {
			if pt == nil {
				return
			}

			if pt.X < set.MinX {
				set.MinX = pt.X
			}
			if pt.Y < set.MinY {
				set.MinY = pt.Y
			}
			if pt.Z < set.MinZ {
				set.MinZ = pt.Z
			}

			if pt.X > set.MaxX {
				set.MaxX = pt.X
			}
			if pt.Y > set.MaxY {
				set.MaxY = pt.Y
			}
			if pt.Z > set.MaxZ {
				set.MaxZ = pt.Z
			}
		}(p)
	}()

	values := strings.Split(str, space)
	if len(values) == 4 {
		return &Point{
			X:             intCoordOf(values[0]),
			Y:             intCoordOf(values[1]),
			Z:             intCoordOf(values[2]),
			RawReflection: intOf(values[3]),
		}
	}
	return nil
}

func (p *Point) String() string {
	return fmt.Sprintf("%d %d %d -> %d", p.X, p.Y, p.Z, p.RawReflection)
}

func intOf(str string) Unit {
	res, _ := strconv.Atoi(str)
	return Unit(res)
}

func intCoordOf(str string) Unit {
	return Unit(float32Of(str) * 10000)
}

func float32Of(str string) float32 {
	res, _ := strconv.ParseFloat(str, 32)
	return float32(res)
}
