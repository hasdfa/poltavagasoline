package mathModels

type (
	Unit     = int
	RawPoint [3]Unit
)

func NewRawPoint(x, y, z Unit) RawPoint {
	return [3]Unit{x, y, z}
}

func (raw RawPoint) X() Unit {
	return raw[0]
}

func (raw RawPoint) Y() Unit {
	return raw[1]
}

func (raw RawPoint) Z() Unit {
	return raw[2]
}

func (raw RawPoint) AddX(delta Unit) RawPoint {
	raw[0] += delta
	return raw
}

func (raw RawPoint) AddY(delta Unit) RawPoint {
	raw[1] += delta
	return raw
}

func (raw RawPoint) AddZ(delta Unit) RawPoint {
	raw[2] += delta
	return raw
}

func Max(u1, u2 Unit) Unit {
	if u1 > u2 {
		return u1
	}
	return u2
}
