package mathModels

type Matrix3D struct {
	arr []Unit

	_MaxX Unit
	_MaxY Unit
	_MaxZ Unit
}

func New3D(maxX, maxY, maxZ Unit) (m *Matrix3D) {
	max := uint64(maxX)*uint64(maxY)*uint64(maxZ) + 1
	m = &Matrix3D{
		arr:   make([]Unit, max),
		_MaxX: maxX,
		_MaxY: maxY,
		_MaxZ: maxZ,
	}
	return
}

func (m *Matrix3D) MaxX() Unit {
	return m._MaxX
}

func (m *Matrix3D) MaxY() Unit {
	return m._MaxY
}

func (m *Matrix3D) MaxZ() Unit {
	return m._MaxZ
}

func (m *Matrix3D) Length() int {
	return len(m.arr)
}

func (m *Matrix3D) Get(x, y, z Unit) Unit {
	return Get(m.arr, m._MaxX, m._MaxY, x, y, z)
}

func (m *Matrix3D) Index(x, y, z Unit) Unit {
	return Index(m._MaxX, m._MaxY, x, y, z)
}

func (m *Matrix3D) Exists(x, y, z Unit) (r bool) {
	return Exists(m.arr, m._MaxX, m._MaxY, m._MaxZ, x, y, z)
}

func (m *Matrix3D) At(index Unit) Unit {
	return m.arr[index]
}

func (m *Matrix3D) Set(x, y, z Unit, val Unit) {
	Set(m.arr, m._MaxX, m._MaxY, x, y, z, val)
}

func (m *Matrix3D) AtPtr(x, y, z Unit) *Unit {
	return AtPtr(m.arr, m._MaxX, m._MaxY, x, y, z)
}
