package mathModels

type IMatrix interface {
	Set(x, y, z, value Unit)
	Get(x, y, z Unit) Unit

	MaxX() Unit
	MaxY() Unit
	MaxZ() Unit

	Exists(x, y, z Unit) bool
	Length() int
}
