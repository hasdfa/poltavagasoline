package mathModels

// indexing based on http://stackoverflow.com/a/20266350/145587
func Index(maxX, maxY, x, y, z Unit) Unit {
	// DEPTH dimension corresponds to z
	// HEIGHT to y
	// WIDTH to x
	//
	// index = HEIGHT * WIDTH * [z] + WIDTH * [y] + [x]
	//     ⬇⬇⬇
	return maxY*maxX*z + maxX*y + x
}

func Exists(arr []Unit, maxX, maxY, maxZ, x, y, z Unit) (r bool) {
	if x < 0 || y < 0 || z < 0 {
		return
	}
	if x >= maxX || y >= maxY || z >= maxZ {
		return
	}
	return Get(arr, maxX, maxY, x, y, z) == 0
}

func Get(arr []Unit, maxX, maxY, x, y, z Unit) Unit {
	return arr[Index(maxX, maxY, x, y, z)]
}
func Set(arr []Unit, maxX, maxY, x, y, z Unit, val Unit) {
	arr[Index(maxX, maxY, x, y, z)] = Max(val, 1)
}

func AtPtr(arr []Unit, maxX, maxY, x, y, z Unit) *Unit {
	return &arr[Index(maxX, maxY, x, y, z)]
}
