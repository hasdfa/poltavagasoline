package matrix

import (
	"fmt"
	"poltavaGasoline/models/mathModels"
	"sync"
)

type (
	M3D struct {
		arr [][][]*Elem

		MaxX mathModels.Unit
		MaxY mathModels.Unit
		MaxZ mathModels.Unit
	}

	Elem struct {
		sync.Mutex
		Value   mathModels.Unit
		Visited bool
	}
)

func New(maxX, maxY, maxZ mathModels.Unit) (m *M3D) {
	m = &M3D{
		arr:  make([][][]*Elem, maxX),
		MaxX: maxX,
		MaxY: maxY,
		MaxZ: maxZ,
	}
	return
}

func (m *M3D) Exists(x, y, z mathModels.Unit) (b bool) {
	if x < 0 || y < 0 || z < 0 {
		return
	}
	if x >= m.MaxX || y >= m.MaxY || z >= m.MaxZ {
		return
	}

	if m.arr[x] == nil {
		return false
	}

	if m.arr[x][y] == nil {
		return false
	}

	return m.arr[x][y][z] != nil
}

func (m *M3D) Get(x, y, z mathModels.Unit) (e *Elem) {
	if x < 0 || y < 0 || z < 0 {
		return
	}
	if x >= m.MaxX || y >= m.MaxY || z >= m.MaxZ {
		return
	}

	if m.arr[x] == nil {
		return nil
	}

	if m.arr[x][y] == nil {
		return nil
	}

	return m.arr[x][y][z]
}

func (m *M3D) Set(x, y, z mathModels.Unit, val mathModels.Unit) {
	defer func() {
		if recover() != nil {
			fmt.Println("PANIC", x, y, z, val, m.MaxX, m.MaxY, m.MaxZ)
		}
	}()
	if x < 0 || y < 0 || z < 0 {
		return
	}
	if x >= m.MaxX || y >= m.MaxY || z >= m.MaxZ {
		return
	}

	if m.arr[x] == nil {
		m.arr[x] = make([][]*Elem, m.MaxY)
	}

	if m.arr[x][y] == nil {
		m.arr[x][y] = make([]*Elem, m.MaxZ)
	}

	if m.arr[x][y][z] == nil {
		m.arr[x][y][z] = &Elem{}
	}

	m.arr[x][y][z].Value = val
}
