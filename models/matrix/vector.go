package matrix

type (
	VectorDirection int
)

const (
	VectorDirectionTop VectorDirection = iota
	VectorDirectionTopLeft
	VectorDirectionLeft
	VectorDirectionLeftDown
	VectorDirectionDown
	VectorDirectionDownRight
	VectorDirectionRight
	VectorDirectionTopRight
)

func VectorCanGoTo(dir VectorDirection, dirs []VectorDirection) {

}
