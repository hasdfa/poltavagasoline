package main

import (
	"flag"
	"fmt"
	"poltavaGasoline/holder"
	"poltavaGasoline/services/jobService"
	"strings"
)

const (
	ApplicationName   = "Gas 3D"
	ApplicationFolder = "APP"
)

var (
	filePath = flag.String("file", "./source/file.xyz", "")
)

func init() {
	flag.Parse()
}

func main() {
	if !strings.HasSuffix(*filePath, ".xyz") {
		panic("Only .xyz files now are supported")
	}
	jobService.PushWatcher(logger)

	set, err := holder.ParseService.ReadXYZFile(*filePath)
	if err != nil {
		fmt.Println(err)
		return
	}
	matrix := holder.ParseService.Fill3D(set)
	count := holder.MathService.CountFromCenter(matrix)
	fmt.Println(count)
}

func logger(status *jobService.ServiceStatus) {
	if status.Status == "Working" {
		fmt.Print("\r" + status.String())
	} else {
		fmt.Println("\r" + status.String())
	}
}
